import java.awt.Color;
import java.awt.Graphics;

public class BodyPart {
    
    private int xCoor, yCoor, width, height;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
        
    public BodyPart(int xCoor, int yCoor, int tileSize, String type){
        this.xCoor = xCoor;
        this.yCoor = yCoor;
        width = tileSize;
        height = tileSize;
        this.type = type;
    }
    
    public void tick(){
        
    }
    
    public void draw (Graphics grap){
        if(this.type == "Comum") {
            grap.setColor(Color.CYAN);
        }else if(this.type == "Kitty") {
            grap.setColor(Color.MAGENTA);
	}else if(this.type == "Star") {
            grap.setColor(Color.YELLOW);
	}
        
        grap.fillRect(xCoor * width, yCoor * height, width, height);
    }

    public int getxCoor() {
        return xCoor;
    }

    public void setxCoor(int xCoor) {
        this.xCoor = xCoor;
    }

    public int getyCoor() {
        return yCoor;
    }

    public void setyCoor(int yCoor) {
        this.yCoor = yCoor;
    }
    
}
