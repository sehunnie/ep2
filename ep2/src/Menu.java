import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

public class Menu {

    private JFrame frame;

    public static void main(String[] args) {
	EventQueue.invokeLater(new Runnable() {
            public void run() {
		try {
                    Menu window = new Menu();
                    window.frame.setVisible(true);
		} catch (Exception e) {
                    e.printStackTrace();
		}
            }
	});
    }

    public Menu() {
	initialize();
    }

    private void initialize() {
        
        //Inicialização do Frama
	frame = new JFrame();
	frame.setBounds(100, 100, 366, 385);
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.getContentPane().setLayout(null);
	frame.setLocationRelativeTo(null);
        frame.setVisible(true);
		
	//Botão para jogar
	JButton btnPlay = new JButton("PLAY");
	btnPlay.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
		new Game();
		frame.setVisible(false);
            }
	});
	btnPlay.setBounds(131, 180, 100, 25);
	frame.getContentPane().add(btnPlay);
		
	//Botão das configurações
	JButton btnTutorial = new JButton("OPTIONS");
	btnTutorial.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
		new About();
		frame.setVisible(false);
            }
	});
	btnTutorial.setBounds(131, 230, 100, 25);
	frame.getContentPane().add(btnTutorial);
		
		
	JLabel lblNewLabel = new JLabel("");
	Image img = new ImageIcon(this.getClass().getResource("menu2.png")).getImage();
	lblNewLabel.setIcon(new ImageIcon(img));
	lblNewLabel.setBounds(0, 0, 350, 350);
	frame.getContentPane().add(lblNewLabel);
    }
}
