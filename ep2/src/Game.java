import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Game {

    private JFrame frame;

    public static void main(String[] args) {
	EventQueue.invokeLater(new Runnable() {
            public void run() {
		try {
                    Game window = new Game();
                    window.frame.setVisible(true);
		} catch (Exception e) {
                    e.printStackTrace();
		}
            }
	});
    }

    public Game() {
	initialize();
    }

    private void initialize() {
	frame = new JFrame();
	frame.setBounds(100, 100, 616, 439);
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.getContentPane().setLayout(null);
	frame.setLocationRelativeTo(null);
        
	//Cobra Comum
	JButton btnComum = new JButton("Comum");
	btnComum.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
		new Main("Comum");
		frame.setVisible(false);
            }
	});
	btnComum.setBounds(50, 210, 100, 30);
	frame.getContentPane().add(btnComum);
		
        //Cobra Kitty
	JButton btnKitty = new JButton("Kitty");
	btnKitty.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
            	new Main("Kitty");
            	frame.setVisible(false);
            }
	});
	btnKitty.setBounds(255, 210, 100, 30);
	frame.getContentPane().add(btnKitty);
		
        //Cobra Star
	JButton btnStar = new JButton("Star");
	btnStar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
		new Main("Star");
		frame.setVisible(false);
            }
	});
	btnStar.setBounds(455, 210, 100, 30);
	frame.getContentPane().add(btnStar);
	frame.setVisible(true);
        
		
	JLabel lblNewLabel = new JLabel("");
	Image img = new ImageIcon(this.getClass().getResource("nyan.png")).getImage();
	lblNewLabel.setIcon(new ImageIcon(img));
	lblNewLabel.setBounds(0, 0, 600, 400);
	frame.getContentPane().add(lblNewLabel);
	
    }
	
}
