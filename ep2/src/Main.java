import javax.swing.JFrame;

public class Main {
    
    private String type;
    
    public Main(String type){
        JFrame frame = new JFrame();
        Panel panel = new Panel(type);
        
        frame.add(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("COBRINHA");
        
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        
    }
    
}
