import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JPanel;

public class Panel extends JPanel implements Runnable, KeyListener{
    private static final long serialVersionUID = 1L;
    
    //Dimensões da Janela
    public static final int WIDTH = 500, HEIGHT = 500;
    
    //Thread
    private Thread thread;
    
    //Running
    private boolean running;
    
    //Variáveis para dar movimento a cobra
    private boolean right = true, left = false;
    private boolean up = false, down = false;
    
    //Variáveis da Cobra
    private BodyPart body;
    private ArrayList<BodyPart> snake;
    private String type;
    
    //Variáveis das Frutas
    private Fruit fruit;
    private ArrayList<Fruit> fruits;
    private ArrayList<Fruit> obstaculos;
    private Random rand;
    private long time;
    
    
    //Variáveis da Classe
    private int xCoor = 10,yCoor = 10, size = 5;
    private int ticks = 0;
    private int score = 0;
    
    public Panel(String type){
        
        setFocusable(true);
        
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        addKeyListener(this);
        
        this.snake = new ArrayList<BodyPart>();
        this.fruits = new ArrayList<Fruit>();
        this.obstaculos = new ArrayList<Fruit>();
        
        rand = new Random();
        this.type = type;
        start();
    }
    
    //Método para o jogo começar 
    public void start(){
        running = true;
        thread = new Thread(this);
        thread.start();
        
    }
    
    //Método para o jogo parar
    public void stop(){
            running = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
           e.printStackTrace();
        }
        
    }
    
    public void tick(){
        //Geração da Cobra
        if(snake.size()==0){ 
            body = new BodyPart(xCoor, yCoor, 10, type);
            snake.add(body);
        }
        ticks++;
        
        if(ticks > 700000){
            if(right) xCoor++;
            if(left) xCoor--;
            if(up) yCoor--;
            if(down) yCoor++;
            
            ticks = 0;
            
            body = new BodyPart(xCoor, yCoor, 10, type);
            snake.add(body);
            
            if(snake.size() > size){
                snake.remove(0);
            }
        }
        
        //Geração do fruto
        if(fruits.size() == 0){
            int j = 0;
            
            int xCoor = rand.nextInt(49);
            int yCoor = rand.nextInt(49);
            
            for(int i = 0; i < obstaculos.size(); i++){
                if(xCoor == obstaculos.get(i).getxCoor() && yCoor == obstaculos.get(i).getyCoor()){
                    j++;
		}
					
            }
				
            for(int i = 0; i < snake.size(); i++){
		if(xCoor == snake.get(i).getxCoor() && yCoor == snake.get(i).getyCoor()){
                    j++;
		}
					
            }
				
            if(j == 0){
					
		fruit = new Fruit(xCoor, yCoor, 10, "simple");
		fruits.add(fruit);
					
		xCoor = rand.nextInt(49);
		yCoor = rand.nextInt(49);
					
		fruit = new Fruit(xCoor, yCoor, 10);
		fruits.add(fruit);
					
		time = System.currentTimeMillis();
            }
        }
        
        if(time + 7000 == System.currentTimeMillis()) {
            fruits.removeAll(fruits);
	}

        
        //Colisão com os frutos
        for(int i = 0; i < fruits.size(); i++){
            if(xCoor == fruits.get(i).getxCoor() && yCoor == fruits.get(i).getyCoor()){
                
                if(fruits.get(i).getType() == "bomb"){
                    stop();
		}
		else if(fruits.get(i).getType() == "decrease"){
                    size = 5;
                
                    while(snake.size()!=5){
                        snake.remove(0);
                    }
		}
                else if (fruits.get(i).getType() == "big"){
                    size +=2;
                }
                else{
                    size ++;
		}
		
                if(body.getType() == "Star"){
                    this.score += (fruits.get(i).getScore()*2);
		}
                else if(body.getType()=="Comum" || body.getType() == "Kitty"){
                    this.score += fruits.get(i).getScore();
		}
                
		fruits.removeAll(fruits);
                
		i++;
            }
        }
        
        //Colisão com o corpo da cobra
        for (int i = 0; i < snake.size(); i++) {
            if(xCoor == snake.get(i).getxCoor() && yCoor == snake.get(i).getyCoor()){
		if(i != snake.size() - 1) {
                    stop();
		}
            }
	}
        
        //Colisão com a borda - Star atravessa e aparece do outro lado
	if(body.getType()=="Comum"||body.getType()=="Star") {
            if(xCoor < 0) {
                xCoor = 59;
            }
            
            if(xCoor > 59) {
		xCoor = 0;
            }
            
            if(yCoor < 0) {
		yCoor = 59;
            }
            
            if(yCoor > 59) {
		yCoor = 0;
            }
        }
        
        //Colisão com a borda - Comum e Kitty: Game Over
        else{
            if(xCoor < 0 || xCoor >59 ||yCoor < 0 || yCoor >59) {
                stop();
            }
	}
    }
    
    public void paint(Graphics grap){
        
        //Campo da Cobra
        grap.clearRect(0, 0, WIDTH, HEIGHT);
        grap.setColor(Color.BLACK);
        grap.fillRect(0, 0, WIDTH, HEIGHT);
        
        //Campo Quadriculado
        for(int i = 0; i < WIDTH/10 ; i++){
            grap.drawLine(i * 10, 0, i * 10, HEIGHT);
        }
        
        
        for(int i = 0; i < HEIGHT/10 ; i++){
            grap.drawLine(0, i * 10, HEIGHT, i * 10);
        }
         
        //Criação dos Obstáculos
	for(int i = 10; i < 20; i++) {	
            fruit = new Fruit(30, i, 10, "obstaculo");
            obstaculos.add(fruit);
            
            fruit = new Fruit(i, 40, 10, "obstaculo");
            obstaculos.add(fruit);
	}
        
	for(int i = 20; i < 40; i++) {
            fruit = new Fruit(30, i, 10, "obstaculo");
            obstaculos.add(fruit);
            
            fruit = new Fruit(i, 15, 10, "obstaculo");
            obstaculos.add(fruit);
	}
        
	for(int i = 0; i < 10; i++) {
            fruit = new Fruit(10, i, 10, "obstaculo");
            obstaculos.add(fruit);
            
            fruit = new Fruit(i, 25, 10, "obstaculo");
            obstaculos.add(fruit);
	}

	for(int i = 0; i < obstaculos.size(); i++){
            obstaculos.get(i).draw(grap);
        }
        
	//Colisão em Obstaculo
        for(int i = 0; i < obstaculos.size(); i++) {
            if(this.type == "Kitty") {
                break;
            }
            if(xCoor == obstaculos.get(i).getxCoor() && yCoor == obstaculos.get(i).getyCoor()) {
                stop();
            }
        }
		
        //Criação da Cobra
        for(int i = 0; i < snake.size(); i++) {
            snake.get(i).draw(grap);	
        }
		
	//Criação das frutas
        for(int i = 0; i < fruits.size(); i++) {
            fruits.get(i).draw(grap);	
	}
		
	//Criação do Score
	grap.setColor(Color.WHITE);
	grap.setFont(new Font("arial", Font.BOLD, 14));
	grap.drawString("Score: "+ score, 10, 15);	
		
		
	if(running == false) {
            
            
            grap.clearRect(0, 0, WIDTH, HEIGHT);
            grap.setColor(Color.BLACK);
            grap.fillRect(0, 0, WIDTH, HEIGHT);

            //Game Over
            grap.setColor(Color.RED);
            grap.setFont(new Font("arial", Font.BOLD, 50));
            grap.drawString("GAME OVER", 100, 190);
			
            //Score final
            grap.setColor(Color.YELLOW);
            grap.setFont(new Font("arial", Font.BOLD, 30));
            grap.drawString("SCORE: "+ score, 180, 250);
			
            //Recomeçar o jogo
            grap.setColor(Color.DARK_GRAY);
            grap.setFont(new Font("arial", Font.BOLD, 20));
            grap.drawString("APERTE ESPAÇO PARA REINICIAR", 85, 400);
			
            grap.dispose();
            grap.clearRect(10, 10, WIDTH, HEIGHT);

	}
		
		
    }


    @Override
    public void run() {
      while(running){
          tick();
          repaint();
          
      }  
    }

    //Movimento para a cobra
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        if(key == KeyEvent.VK_RIGHT && !left) {
            right = true;
            up = false;
            down = false;
        }

        if(key == KeyEvent.VK_LEFT && !right) {
            left = true;
            up = false;
            down = false;
        }

        if(key == KeyEvent.VK_UP && !down) {
            up = true;
            left = false;
            right = false;
        }

        if(key == KeyEvent.VK_DOWN && !up) {
            down = true;
            left = false;
            right = false;
        }
        
        if(e.getKeyCode() == KeyEvent.VK_SPACE) {
            score = 0;
            new Game();
	}

		
    }

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}    
}
