import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;

public class Fruit {
    
    private int xCoor, yCoor, width, height;
    private int score;
    private String type;	

    
    public Fruit(int xCoor, int yCoor, int tileSize){
        this.xCoor = xCoor;
        this.yCoor = yCoor;
        this.width = tileSize;
        this.height = tileSize;
        
        ArrayList<String> types = new ArrayList<String>();
	types.add("simple");
	types.add("big");
	types.add("decrease");
	types.add("bomb");
        
        Random rand = new Random();
	int r = rand.nextInt(4);
		
	this.type = types.get(r);
		
		
	if(type == "simple") {
            this.score = 1;
	}
	else if(type == "big") {
            this.score = 2;
	}
	else if(type == "decrease") {
            this.score = 0;
	}
	else if(type == "bomb") {
            this.score = 0;
        }
    }
    
    public Fruit(int xCoor, int yCoor, int tileSize, String type) {
	this.xCoor = xCoor;
	this.yCoor = yCoor;
	this.height = tileSize;
	this.width = tileSize;
		
	this.type = type;		
			
	if(type == "simple") {
            this.score = 1;
	}
	else if(type == "big") {
            this.score = 2;
	}
        else if(type == "decrease") {
            this.score = 0;
	}
	else if(type == "bomb") {
            this.score = 0;
	}
	else if(type == "obstaculo") {
            this.score = 0;
	}
		
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
	
        
    public void tick(){
        
    }
    
    public void draw (Graphics grap){
        if(type == "simple") {
            grap.setColor(Color.PINK);
	}
        else if(type == "bomb") {
            grap.setColor(Color.WHITE);
	}
        else if(type == "big") {
            grap.setColor(Color.RED);
	}
        else if(type == "decrease") {
            grap.setColor(Color.BLUE);
        }
        else if(type == "obstaculo") {
            grap.setColor(Color.LIGHT_GRAY);
	}

        grap.fillRect(xCoor * width, yCoor * height, width, height);
    }

    public int getxCoor() {
        return xCoor;
    }

    public void setxCoor(int xCoor) {
        this.xCoor = xCoor;
    }

    public int getyCoor() {
        return yCoor;
    }

    public void setyCoor(int yCoor) {
        this.yCoor = yCoor;
    }
    
    
}