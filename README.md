# EP2 - Jogo da Cobrinha
* Aluna: Juliana Pereira Valle Gonçalves
* Matrícula: 18/0124099 

## Descrição
O clássico joguinho da cobrinha voltou com uma proposta revolucionária. Na verdade... não... é o mesmo joguinho que você conhece, só que mais fofo. Disponíveis para todas plataformas (menos IOS) e, em breve, na Steam.

## Tipos de Cobrinhas
* **Comum:** A classica, sem habilidades especiais.
* **Kitty:** A fofa, possui habilidades de atravessar as barreiras do jogo, mas não pode atravessar as bordas e nem a si mesma.
* **Star:** A gulosa, recebe o dobro de pontos ao comer as frutas.

## Frutas
* **Simple Fruit:** Fruta comum, dá um ponto e aumenta o tamanho da cobra.
* **Bomb Fruit:** Essa fruta deve levar a morte da Snake.
* **Big Fruit:** Dá o dobro de pontos da Simple Fruit e aumenta o tamanho da cobra da mesma forma que a Simple Fruit.
* **Decrease Fruit:** Diminui o tamanho da cobra para o tamanho inicial, sem fornecer nem retirar pontos.

## Como jogar
* **Movimentação:** A cobrinha pode ser controlada por atráves das setinhas do teclado.
* **Jogar Novamente:** Na tela de Game Over, é possível reiniciar o jogo apertando a tecla espaço.

**As instruções do jogo podem ser consultadas no menu inicial > options.**

## Sobre o Jogo
Esse jogo pode ser inicializado executando o arquivo "SnakeGame.jar". Esse arquivo se encontra em scr > executavel.
Outra forma de inicializar é abrir o projeto no NetBeans e complicar a classe "Menu.java".

Este jogo foi desenvolvido na linguagem Java utilizando a IDE NetBeans 8.2.

#Diagrama de Classes
O diagrama de classe pode acessado através do link: https://prnt.sc/pw6ek4
